import "@/styles/globals.css";
import { theme } from "@/utils/theme";

import type { AppProps } from "next/app";
import { ThemeProvider } from "@mui/styles";


export default function App({ Component, pageProps }: AppProps) {
  return <ThemeProvider theme={theme}>
  <Component {...pageProps} />
</ThemeProvider>;;
}
