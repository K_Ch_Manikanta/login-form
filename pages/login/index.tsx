import DynamicFormComponent from "@/components/DynamicForm";
import { isFieldsValidate, formValidationSchema } from "@/helpers/login";
import {
  formFields,
  initialValues,
  mandatoryFields,
} from "@/helpers/formFieldsData";
import useReactForm from "@/hooks/useReactHookForm";
import { IToken } from "@/interfaces/login";
import { Box, Typography, Paper } from "@mui/material";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { users } from "@/utils/dummyData";
import ToastMessage from "@/components/ToastMessage/ToastComponent";
import loginStyles from "./styles.module.css";
import { LOGIN_URL } from "@/utils/endpoints";

export default function LoginForm() {
  const router = useRouter();
  const { errors, setValue, getValues, watch, control } = useReactForm(
    initialValues,
    formValidationSchema,
    ""
  );
  const [openAlert, setOpenAlert] = useState(false);

  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<IToken | any>({});
  const [customErrorMessage, setCustomErrorMessage] = useState("");

  useEffect(() => {
    const fieldValues = JSON.parse(localStorage.getItem("fieldValues") || "{}");
    setValue("full_name", fieldValues?.full_name);
    setValue("email", fieldValues?.email);
    setValue("gender", fieldValues?.gender);
    setValue("love_react", fieldValues?.love_react);
  }, []);

  useEffect(() => {
    if (data && data !== undefined && Object.keys(data).length > 0) {
      sessionStorage.setItem("token", data?.token);
      setLoading(false);
      router.push(`/dashboard/${watch().full_name}`);
    }
  }, [data]);

  const handleClose = () => {
    setOpenAlert(false);
    setLoading(false);
  };

  const handleLogin = async () => {
    setLoading(true);
    const values = watch();
  
    const userName = users.find((user) => user.name === values.full_name);
  
    if (!userName) {
      setCustomErrorMessage("Incorrect Full Name");
      setOpenAlert(true);
      setLoading(false);
      return;
    }
  
    const userObj = users.find((user) => user.email === userName.email);
  
    if (!userObj) {
      setCustomErrorMessage("Incorrect Email");
      setOpenAlert(true);
      setLoading(false);
      return;
    }
  
    if (userObj.email !== values.email || userObj.gender !== values.gender) {
      setCustomErrorMessage("Incorrect Email or Gender");
      setOpenAlert(true);
      setLoading(false);
      return;
    }
  
    localStorage.setItem("fieldValues", JSON.stringify(watch()));
  
    try {
      const response = await fetch(`${LOGIN_URL}login`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          username: "kminchelle",
          password: "0lelplR",
        }),
      });
  
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error("Error during login:", error);
      setCustomErrorMessage("Error during login");
      setOpenAlert(true);
    } finally {
      setLoading(false);
    }
  };
  

  console.log("watcch values", watch());

  return (
    <>
      <Box className={loginStyles.container}>
        <Box className={loginStyles.contentBox}>
          <Box className={loginStyles.paperBox}>
            <Paper elevation={3} style={{ padding: 16 }}>
              <Typography variant="h6" className={loginStyles.title}>
                Login
              </Typography>
              <DynamicFormComponent
                fields={formFields}
                errors={errors}
                setValue={setValue}
                getValues={getValues}
                watch={watch}
                control={control}
                loading={loading}
                disable={!isFieldsValidate(errors, mandatoryFields, getValues)}
                handleLogin={() => handleLogin()}
              />
            </Paper>
          </Box>
        </Box>
      </Box>
      {openAlert && (
        <ToastMessage
          open={true}
          handleClose={handleClose}
          message={customErrorMessage}
          type="error"
        />
      )}
    </>
  );
}
