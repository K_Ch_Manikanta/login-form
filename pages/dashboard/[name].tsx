import Dashboard from "@/components/Dashboard";
import { Container } from "@mui/material";

import React from "react";

export default function HomePage() {
  return (
    <Container>
      <Dashboard />
    </Container>
  );
}
