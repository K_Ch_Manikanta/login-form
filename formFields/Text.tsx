import { FormControl, FormLabel, Grid, TextField } from "@mui/material";
import { Controller } from "react-hook-form";
import ErrorMessage from "@/components/DynamicForm/ErrorMessage";

interface ITextFieldProps {
  field: any;
  control: any;
  errors: any;
  name?: any;
  indexNumber?: any;
}

const TextFieldComponent = ({
  field,
  control,
  errors,
  name,
  indexNumber,
}: ITextFieldProps) => {
  const {
    id,
    breakpoints,
    defaultValue,
    name: fieldName,
    label,
    type,
    className,
    style,
    disabled,
  } = field;

  return (
    <Grid item {...breakpoints} sx={{ padding: "16px", border: "none" }}>
      <Controller
        defaultValue={defaultValue}
        name={id}
        control={control}
        render={({ field: { onChange, value } }) => (
          <FormControl component="fieldset" fullWidth>
            <FormLabel component="legend" sx={{ mb: "12px" }}>
              {fieldName}
            </FormLabel>
            <TextField
              onChange={onChange}
              value={value || ""}
              label={label}
              type={type}
              className={className}
              fullWidth
              variant="outlined"
              sx={{ width: "100%", ...style }}
              size="small"
              disabled={disabled}
            />
            {errors && errors[id] && (
              <ErrorMessage
                field={field}
                errors={errors}
              />
            )}
          </FormControl>
        )}
      />
    </Grid>
  );
};

export default TextFieldComponent;
