import { FormControl, Grid, FormLabel, RadioGroup, FormControlLabel, Radio } from "@mui/material";
import { Controller} from "react-hook-form";

interface IRadioFieldProps {
  field: {
    id: string;
    breakpoints: any;
    name: string;
    listOfValues1: string[];
    defaultValue: string;
  };
  control: any;
}

const RadioField = ({ field, control }: IRadioFieldProps) => {
  const { id, breakpoints, name, listOfValues1, defaultValue } = field;

  return (
    <Grid item {...breakpoints} sx={{ padding: "16px" }}>
      <Controller
        render={({ field: { onChange, value } }) => (
          <FormControl component="fieldset">
            <FormLabel component="legend" sx={{ mb: "12px" }}>
              {name}
            </FormLabel>
            <RadioGroup
              row
              aria-label={id}
              name={id}
              value={value}
              onChange={onChange}
            >
              {listOfValues1.map((radiofield: string, index: number) => (
                <FormControlLabel
                  key={index}
                  value={radiofield}
                  control={<Radio />}
                  label={radiofield}
                />
              ))}
            </RadioGroup>
          </FormControl>
        )}
        name={id}
        control={control}
        defaultValue={defaultValue}
      />
    </Grid>
  );
};

export default RadioField;

