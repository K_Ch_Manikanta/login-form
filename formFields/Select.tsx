import ErrorMessage from "@/components/DynamicForm/ErrorMessage";
import { Grid, FormControl, InputLabel, Select, MenuItem, FormLabel } from "@mui/material";
import { Controller } from "react-hook-form";

interface ISingleSelectCustomFieldProps {
  field: any;
  control: any;
  errors?: any;
}

const ListField = ({ field, errors, control }: ISingleSelectCustomFieldProps) => {
  const { id, breakpoints, label, name, required, listOfValues1 } = field;

  return (
    <Grid item {...breakpoints} sx={{ padding: "16px" }}>
      <FormControl
        error={errors && errors[id]?.message}
        component="fieldset"
        size="small"
        fullWidth
      >
        <InputLabel id={`label-${id}`}>{label}</InputLabel>
        <FormLabel component="legend" sx={{ mb: "10px" }}>{name}</FormLabel>
        <Controller
          render={({ field: { onChange, value } }) => (
            <Select
              labelId={`label-${id}`}
              id={`select-${id}`}
              value={value}
              label={label}
              onChange={onChange}
              placeholder={label}
              required={required}
            >
              <MenuItem value="">None</MenuItem>
              {listOfValues1?.map((option: string) => (
                <MenuItem value={option} key={option}>
                  {option}
                </MenuItem>
              ))}
            </Select>
          )}
          name={id}
          control={control}
          defaultValue=""
        />
        {errors && errors[id]?.message && <ErrorMessage field={field} errors={errors} />}
      </FormControl>
    </Grid>
  );
};

export default ListField;
