
export const mandatoryFields = [
    { id: "full_name" },
    { id: "email" },
    { id: "gender" },
]

export const initialValues = {
    full_name: "",
    email: "",
    gender: "",
    love_react: false
}

export const formFields = [
    {
        id: "full_name",
        name: "Full Name *",
        fieldType: "TEXT",
        minLength: 1,
        type: "text",
        maxLength: 50,
        defaultValue: "Jack",
        required: true,
        label: "Enter Your Full Name",
        breakpoints: { xs: 12, sm: 6, md: 4, lg: 3 },
    },
    {
        id: "email",
        name: "Email *",
        fieldType: "TEXT",
        minLength: 1,
        maxLength: 30,
        type: "email",
        defaultValue: "react@mail.com",
        required: true,
        label: "Enter Your Email",
        breakpoints: { xs: 12, sm: 6, md: 4, lg: 3 },
    },
    {
        id: "gender",
        name: "Gender *",
        fieldType: "LIST",
        defaultValue: 1,
        required: true,
        listOfValues1: [
            "Male",
            "Female",
            "Others"
        ],
        label: "Select Gender",
        breakpoints: { xs: 12, sm: 6, md: 4, lg: 3 },
    },
    {
        id: "love_react",
        name: "Love React ?",
        fieldType: "RADIO",
        defaultValue: "Yes",
        required: false,
        listOfValues1: [
            "Yes",
            "No"
        ],
        breakpoints: { xs: 12, sm: 6, md: 4, lg: 3 },
    }
]
