
import * as Yup from "yup";

const regexValidation =
  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export const formValidationSchema = Yup.object().shape({
  full_name: Yup.string()
    .max(50, "Maximum characters length is 50")
    .required("Full Name is required"),
  email: Yup.string()
    .max(50, "Maximum characters length is 30")
    .required("Email is required")
    .matches(regexValidation, "Please enter a valid email address"),
  gender: Yup.string().required("Gender is required"),
  love_react: Yup.string(),
});

export const isFieldsValidate = (
  errors: any,
  fields: { id: string }[],
  getValues: any
) => {
  const emptyFields = fields.map(({ id }) => getValues(id));
  const allErrorEmpty = fields.map(({ id }) => errors[id]?.message || "");

  const isEveryFieldNotEmpty = emptyFields.every((field: any) => {
    if (Array.isArray(field)) return field.length > 0;
    return field !== "" && field !== undefined && field !== false;
  });

  const isEveryErrorMessageEmpty = allErrorEmpty.every(
    (field: any) => field === ""
  );

  return (
    isEveryFieldNotEmpty &&
    isEveryErrorMessageEmpty &&
    Object.keys(errors).length === 0
  );
};
