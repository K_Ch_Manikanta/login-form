import { IDynamicFieldProps } from "@/interfaces/login";
import {Wrapper,ErrorMsg} from './StyledComponents'

interface ErrorMessageProps {
    field: IDynamicFieldProps;
    errors: any;
    
}
const ErrorMessage = ({ field,errors }: ErrorMessageProps) => {
    return (
        <Wrapper>
            <ErrorMsg>
                {errors &&
                    errors?.[field.id]?.message}
            </ErrorMsg>
        </Wrapper>
    );
};

export default ErrorMessage
