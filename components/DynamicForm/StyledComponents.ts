import { styled } from "@mui/styles";

export const ErrorMsg = styled("p")({
    margin: 0,
    padding: "5px 0px 0px 0px ",
    color: "#e21313",
    fontSize: "12px",
});

export const Wrapper = styled("span")({
    width: "100%",
    display: "inline-block",
    marginBottom: "10px",
});