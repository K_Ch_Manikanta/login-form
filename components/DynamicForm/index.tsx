import React from "react";
import TextFieldComponent from "@/formFields/Text";
import RadioField from "@/formFields/Radio";
import ListField from "@/formFields/Select";
import { IDynamicFieldProps } from "@/interfaces/login";
import { LoadingButton } from "@mui/lab";

type FormProps = {
  reset?: (data: Record<string, any>) => void;
  fields: any;
  setValue?: any;
  errors: {};
  control: {};
  getValues: (id: string) => void;
  watch?: {};
  index?: number;
  name?: string;
  loading: boolean;
  disable: boolean;
  handleLogin: () => void;
};

const componentMap: Record<string, React.ComponentType<any>> = {
  TEXT: TextFieldComponent,
  RADIO: RadioField,
  LIST: ListField,
};

export default function DynamicFormComponent(props: FormProps) {
  const { fields, control, errors, watch, handleLogin, disable, loading } =
    props;

  const renderField = (field: IDynamicFieldProps) => {
    const FieldComponent: any = componentMap[field.fieldType];
    if (!FieldComponent) return null;

    return (
      <FieldComponent
        field={field}
        control={control}
        errors={errors}
        watch={watch}
      />
    );
  };
  return (
    <React.Fragment>
      {fields && fields.map(renderField)}
      <LoadingButton
        variant="contained"
        size="medium"
        loadingPosition="center"
        disabled={disable}
        onClick={() => handleLogin()}
        loading={loading}
        sx={{ width: "100%",marginBottom: "20px",backgroundColor: '#ff5733',  color: '#ffffff','&:hover': {
          backgroundColor: '#e64a19'
        }, }}
      >
        Login
      </LoadingButton>
    </React.Fragment>
  );
}
